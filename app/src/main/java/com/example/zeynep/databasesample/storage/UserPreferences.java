package com.example.zeynep.databasesample.storage;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

public class UserPreferences {
    private static UserPreferences instance;
    private SharedPreferences sharedPreferences;
    private String key;

    public UserPreferences(Context context) {
        key = "user_key";
        sharedPreferences = context.getApplicationContext().getSharedPreferences(key, Context.MODE_PRIVATE);
    }
    public static UserPreferences getInstance(Context context) {
        if(instance == null) instance = new UserPreferences(context);
        return instance;
    }
    public void saveUser(UserModel userModel) {

        try {
            Gson gson = new Gson();
            SharedPreferences.Editor editor = sharedPreferences.edit();
            String user = gson.toJson(userModel);
            editor.putString("loggedUser", user);
            editor.apply();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public UserModel readUser() {
        try {
            Gson gson = new Gson();
            String json = sharedPreferences.getString("loggedUser", null);
            return gson.fromJson(json, UserModel.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    public UserModel deleteUser() {
        try {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.remove("loggedUser");
            editor.apply();
        } catch (Exception e) {
            e.printStackTrace();
        }
       return null;
    }
}
